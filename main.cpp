#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <istream>
#include "math.h"
//#include "pGnuPlotU.h"
//#include "stdafx.h"
//#include <winnt.h>

#define ERROR -1
#define AVERAGED 1
#define MEDIUM 2

using namespace std;
typedef vector<string> v1str;
typedef vector<vector<string>> v2str;
 
v1str csv_read_row(istream &file, char delimter);
v2str csv_transpose(v2str &mat);
v2str readfromcsv(istream &file);
void SampleRefcase(vector<string> List,int t_ref,string resname);

int main(){
/*
    ifstream file("C:\\Users\\Desktop\\Data_3case\\M_Z3.csv"); 인식 X
    ifstream file("initialAdminPasswordasdf.txt"); 인식 (같은 working directory)
    ifstream 선언 후 open함수로 열기
    file.open("C:\\User\\Desktop\\initialAdminPassword12.txt"); 인식 X
    file.open("initialAdminPassword.txt"); 인식 X
/**** Make *.CSV file list
    A_B.CSV 
    -> A : Zcam3 / EYEXO / GCQ
    -> B : Version Name
*/

    vector <string> List_trg;
    string cam="Z3";
    string vs[6]={"PR31","PR32","PR33","R242","R254","R258"};

    for(int i=0;i<sizeof(vs)/sizeof(string);i++){
        List_trg.emplace_back(vs[i]+"_"+cam+".csv");
    }

    SampleRefcase(List_trg, AVERAGED,cam+"_averaged_ref.csv");
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//Function for sorting Raw data to pre-processing data 
v1str csv_read_row(istream &fn,char delimeter){
    stringstream ss;
    v1str row;

    while(fn.good()){
        char c=fn.get();
        if(c=='"'){

        }
        else if(c=='\n'){
            row.emplace_back(ss.str());
            return row;
        }
        else if(c==delimeter){
            row.emplace_back(ss.str());
            ss.str(""); // initialize ss
        }
        else{
            ss<<c;
        }
    }
}
v2str csv_transpose(v2str &mat){
    v2str all2(mat[0].size(),v1str(mat.size()-1,"0"));
   
    for(int i=0; i<mat[0].size();i++){ // # of row
        for(int j=0;j<mat.size()-1;j++){ // # of column - withdraw last line containing space
            all2[i][j]=mat[j][i];
        }
    }

    return all2;
}
v2str readfromcsv(istream &file){
    v2str all;
    if(file.fail()){
         cout<<"No file"<<endl;
    }
    else{    
        while(file.good()){
        v1str row=csv_read_row(file,',');
        all.emplace_back(row);
    }

    return csv_transpose(all);
    
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////// 

/*
***** Function for picking up reference case(info.xml >> iana >> case) with respect to various version 
    * Assumption
    - Type of camera is fixed
    - row&column length is fixed for all version
    - Will be added

    * Function input : file list, statistical method, output file name
    * Function output : void
    * Function result : print csv result file 

*/

void SampleRefcase(vector<string> List,int t_ref,string resname){
//------------------------------------------------------------------------------------------------------------    
//---------- Sort Raw data(Version - Variable - Case) to Post(Variable - Version - Case)
    int nlist=List.size();
    vector<v2str> Raw;
    vector<v2str> Post;

    for(int i=0; i<nlist;i++){
        ifstream fn(List[i]);
        Raw.emplace_back(readfromcsv(fn));
    }
    for(int j=0;j<Raw[0].size();j++){
        v2str nvers;
        for(int i=0;i<nlist;i++){
            nvers.emplace_back(Raw[i][j]);
        }
        Post.emplace_back(nvers);     
    }
//cout<<Raw.size()<<endl <<Raw[0].size()<<endl<<Raw[0][0].size()<<endl;
//cout<<Post.size()<<endl <<Post[0].size()<<endl<<Post[0][0].size()<<endl;

//------------------------------------------------------------------------------------------------------------
//---------- Tag Case/Version - Camera + Date
//

// will be added

//------------------------------------------------------------------------------------------------------------ 
    // (1) Make 2D vector for single case {{Var1},{Var2}, ...}
    // (2) Decision making - add reference list or not
    // (3) Make 3D vector - Collect reference list 2D vector

    v2str Refcase;
    vector<string> indexname;
    indexname.emplace_back(Post[0][0][0]);
    for(int i=3;i<Post.size();i++){
        indexname.emplace_back(Post[i][0][0]);
    }
    Refcase.emplace_back(indexname);
    for(int i=1;i<Post[0][0].size();i++){ // for each case
        vector<string> cvar;
        cvar.emplace_back(Post[0][0][i]);
        int Data_tag;
        for(int j=3;j<Post.size();j++){ // for each variable
            double cvarel[nlist];
            for(int c=0;c<nlist;c++){
                cvarel[c]=stod(Post[j][c][i]); //cvarel : 1case variable - multi version
            }
            if(t_ref==AVERAGED){
                double sum=0;
                int cnt=0;
            
                for(int k=0;k<sizeof(cvarel)/sizeof(double);k++){
                    Data_tag=0;
                    if(abs(cvarel[k])>0.0000000001){
                        if(abs(sum)>abs(sum+cvarel[k])){
                            Data_tag=1;
                            break;
                        }
                        else{
                            sum+=cvarel[k]; 
                            cnt++;  
                        }
                    }
                    else{
                       if(cnt>0){
                            Data_tag=0;
                       }
                       else{
                            Data_tag=2;
                       }

                    }
                }
                if(Data_tag==0){
                    cvar.emplace_back(to_string(sum/(cnt)));
                }
                else if(Data_tag==1){
                    cvar.emplace_back("Different Sign");
                }
                else if(Data_tag==2){
                    cvar.emplace_back("Zero");
                }
                else{
                    cvar.emplace_back("Unknown"); // Check for missing something
                }
            }  
        }
        Refcase.emplace_back((cvar)); 
    }

//------------------------------------------------------------------------------------------------------------
//********** Testing Code    
    ofstream all(resname);
    for(int i=0;i<Refcase.size();i++){
        for(int j=0;j<Refcase[0].size();j++){
            all<<Refcase[i][j]<<',';
            cout<<Refcase[i][j];
        }
            all<<endl;
    }
}


